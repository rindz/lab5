<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */

    
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
        Validator::extend('in_phone', function($attribute, $value, $parameters){
            return substr($value,0,3) == '+84';
        });
        
        Validator::extend('ten_year', function($attribute,$value,$parameters,$validator){
            
            $year = Carbon::parse($value)->year;
            $yearnow = Carbon::now()->year-10;
            return $year<=$yearnow;
        });

        
        Validator::extend('id_number',function($attribute, $value, $parameters){
            $id = strlen($value);
            return $id == 9 || $id == 12;
        });
        


        
    }
}
