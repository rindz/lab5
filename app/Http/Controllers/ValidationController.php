<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\MessageBag;

class ValidationController extends Controller
{
    public function index(){
       return view('bai1');
    }
    public function bai1(Request $request){

        $validator = Validator::make($request->all(), [
            'name_login' => 'required',
            'pwd' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('bai1/input')
                        ->withErrors($validator)
                        ->withInput();
        }
        return 'Login Successfull';
    }
    public function bai2(Request $request){

        Validator::make($request->all(), [
            'phone' => 'required|in_phone|min:9',
        ])->validate();
        
        $phone = $request->post('phone');
            return "Your number phone is " .$phone;
    }

    public function bai3(Request $request){
        // dump($request->all());
        Validator::make($request->all(), [
            'name' => 'required|min:6',
            'email' => 'required',
            'date' => 'required|ten_year',
            'phone' => 'required|in_phone|min:10',
            'id_number' => 'required|id_number',
        ])->validate();
        
            return "Registed Succesful";
    }
}
