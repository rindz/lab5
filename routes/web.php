<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/bai1/input',function(){return view('bai1');});

Route::post('/bai1','ValidationController@bai1');


Route::get('/bai2/input',function(){return view('bai2');});

Route::post('/bai2','ValidationController@bai2');


Route::get('/bai3/input',function(){return view('bai3');});

Route::post('/bai3','ValidationController@bai3');